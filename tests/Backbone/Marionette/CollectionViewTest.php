<?php
namespace Backbone\Marionette;

use PHPUnit_Framework_TestCase;
use Backbone\Marionette;
use DOMDocument;

class CollectionViewTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }

    public function testRender()
    {
        $collection = [[],[],[]];

        $renderer = $this->getMock('Renderer', array('render'));

        $renderer
             ->expects($this->exactly(3))
             ->method('render')
             ->willReturn('Item');

        Marionette::$Renderer = $renderer;


        $dom = new DOMDocument();
        $dom->loadXML('<root />');

        $view = new CollectionView([
            'childView'  => 'Backbone\Marionette\ItemView',
            'collection' => $collection,
            'el'=>$dom->documentElement
        ]);

        $view->render();

        $this->assertEquals(
            $view->el->c14n(),
            '<root><div>Item</div><div>Item</div><div>Item</div></root>'
        );
    }
}
