<?php

namespace Backbone\Marionette;

use Backbone\View;

class Region
{
    private $el;

    public function __construct($options) {
        $this->el = $options['el'];
    }

    public function show(View $view) {
        $this->deleteChildren($this->el);

        $el = $this->el->appendChild($this->el->ownerDocument->createElement($view->tagName));

        $view->setElement($el);

        $view->render();
    }

    protected function deleteChildren($node) {
        while (isset($node->firstChild)) {
            $this->deleteChildren($node->firstChild);
            $node->removeChild($node->firstChild);
        }
    }
}

