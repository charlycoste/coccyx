<?php
namespace Backbone\Marionette;

use DOMXPath;

class LayoutView extends ItemView
{
  private $regions;
  public $el;

  public function __construct($options) {

     parent::__construct($options);
     $this->options = $options;
  }

  public function __get($region) {
    if (!array_key_exists($region, $this->regions)) {
        throw new Exception("Region {$region} is unknown");
    }

    return $this->regions[$region];
  }

  public function render()
  {
      parent::render();

      $xpath = new DOMXPath($this->el->ownerDocument);

      foreach ($this->options['regions'] as $region=>$selector) {

        $selector = str_replace('#','',$selector);
        $this->regions[$region] = new Region(['el'=>$xpath->query("//*[@id='{$selector}']")->item(0)]);
      }
  }
}
