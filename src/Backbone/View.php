<?php

namespace Backbone;

use DOMElement;

class View
{
    public $tagName= 'div';
    public $el;
    public $model;
    public $renderer;

    public function __construct($options = [])
    {
        $this->tagName  = empty($options['tagName']) ? $this->tagName : $options['tagName'];
        $this->model    = empty($options['model'])   ? [] : $options['model'];
        $this->renderer = empty($options['renderer']) ? [] : $options['renderer'];
        $this->el       = empty($options['el']) ? [] : $options['el'];
        $this->options  = $options;
    }

    public function render()
    {
        return $this;
    }

    public function setElement(DOMElement $el) {
        $this->el = $el;
    }

    public function remove()
    {
        $this->el->parentNode->removeChild($this->el);
    }
}
