# Coccyx

Coccyx is the last element of the backbone. It's also the last piece of code I needed to share templates
between my frontend javascript code and my backend PHP code.

## Why this project ?

By making use of logic-less templating you can use a template engine like Mustache to code a template only once time
and be able to use it on server side or on client side.

But when you are using BackboneJS views, the rendered HTML will be : "<tagname>{result_of_template}</tagname>" where
tagname is not provided by the template itself but by an attribute of the view object.

So, let's do the same in PHP instead of concatenating hard coded string like `"<div>{$result_of_template}</div>"` :

    Class MyView extends Backbone\View
    {
      public function render()
      {
        $this->el = $this->renderer->render("<h1>{{title}}</h1><p>{{description}}</p>", $this->model);
        return $this;
      }
    }
    
    $view = new MyView([
      'tagName' => 'section',
      'model'=>[
        'title'=>'Hello world',
        'description'=>'Lorem ipsum dolor'
      ]
    ]);
    
    $view->render();
    
    echo $view->el; // -> <section><h1>Hello world</h1><p>Lorem ipsum dolor</p></section>
